let mbCore = require('../../index');

describe('Security', () => {
  let secutiry = mbCore.security;

  it('permissions should be object', done => {
    if (!secutiry.permissions)
      return done('no permissions');
    if (typeof secutiry.permissions !== 'object')
      return done('permissions not object');
    done();
  });


  let permissions = secutiry.permissions;
  it('meetings add permissions', done => {
    console.log(permissions.meetings.add);
    if (permissions.meetings.add !== 'meetings.add')
      return done('wrong permission value');
    done();
  });
});