let permisssions = require('./permissions');
const errors = require('./errors');
let result = {};

const extract = (parentKey, obj) => {
  let res = {};
  Object.keys(obj).forEach(key => {
    let val = obj[key];
    if (typeof  val === 'object')
      res[key] = extract(parentKey ? `${parentKey}.${key}` : key, val);
    else 
      res[key] = `${parentKey}.${val}`;
  });
  return res;
}


const checkPermission = (permissions, permission) => {
  if (!permissions)
    return false;
  return permissions.indexOf(permission) > -1;
};

const getPermissions = permissions => {
  let res = [];
  Object.keys(permissions).forEach(key => {
    let val = permissions[key]
    if (typeof val === 'string')
      res.push(val);
    else {
      let childRes = getPermissions(val);
      childRes.forEach(f => res.push(f));
    }
  });
  return res;
}

const getAllPermissions = () => {
  return getPermissions(permisssions);
}

module.exports = {
  errors: errors,
  checkPermission: checkPermission,
  permissions: permisssions,
  getAllPermissions
}
