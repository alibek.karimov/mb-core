module.exports = {
  questions: require('./questions'),
  meetings: require('./meetings'),
  users: require('./users'),
  roles: require('./roles'),
  other: require('./other')
}
